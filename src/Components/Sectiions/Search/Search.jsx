import React from 'react';
import * as styles from './Search.module.css'

function Search({setChoosedFetchLink, setChoosedDataStatus}) {   

   const search = (event) => {
    const searchWord = event.target.value      
      setChoosedFetchLink(`https://newsapi.org/v2/everything?q=${searchWord}&apiKey=c89f1df91cd34ce9807189edf86192b4`)
            
    if (event.key === 'Enter') {
        setChoosedDataStatus(false)
        event.target.value = ''
        }
    }
    
    return (    
        <section className={styles.search}>
            <div className="container">                
                <div className={styles.searchArticle}>
                    <h2 className={styles.searchTitle}>News Search</h2>

                    <input className="searchInput" onKeyPress={search} name="search" placeholder="Enter news" />
                </div>
            </div>            
        </section>    
      )
}

export default Search;