import React from 'react'
import * as styles from './About.module.css'
import { Carousel } from 'antd';



function About() {


    const contentStyle1 = {
        height: '400px',
        color: '#fff',
        lineHeight: '100px',
        textAlign: 'center',
        background: 'url("./Images/1.jpg") 100% 18% /cover no-repeat'
    };
    const contentStyle2 = {
        height: '400px',
        color: '#fff',
        lineHeight: '100px',
        textAlign: 'center',
        background: 'url("./Images/2.jpg") 100% 18% /cover no-repeat'
    };
    const contentStyle3 = {
        height: '400px',
        color: '#fff',
        lineHeight: '100px',
        textAlign: 'center',
        background: 'url("./Images/3.jpg") 100% 18% /cover no-repeat'


    };
    const contentStyle4 = {
        height: '400px',
        color: '#fff',
        lineHeight: '100px',
        textAlign: 'center',
        background: 'url("./Images/4.jpg") 100% 18% /cover no-repeat'
    };

   

    return (

        <div className={styles.about}>
            <div className="container">
                <div className={styles.wrapper}>
                    <h2>About News</h2>


                    <p>Maecenas lorem arcu pharetra at cursus curabitur adipiscing proin in vitae nibh in. Pellentesque sed arcu metus gravida porttitor bibendum nulla integer, fusce amet tellus quam: sem nibh. Nec proin eget proin et vitae et justo elementum massa maecenas eros ornare nulla sed sit tellus nam sit eget. Nibh maecenas bibendum nulla sagittis eros curabitur ligula, justo in porta orci congue: diam sit ultricies adipiscing elementum, molestie duis donec.</p>

                    <p>Congue odio curabitur sed — tempus quisque pellentesque molestie tempus, sit adipiscing, diam quisque sodales bibendum malesuada. Proin lectus risus ligula eros risus: ipsum proin ligula tempus eu enim non massa sem magna quam. Sem congue eros integer eget, proin: elementum, sem: tellus massa nam arcu, porta risus. Maecenas molestie sed metus mattis congue, quisque ut mattis at ligula malesuada cursus sem magna. Et, molestie sed duis adipiscing nibh gravida lectus diam amet adipiscing. Pellentesque, urna sapien magna ipsum et bibendum sapien: tempus duis at nibh — porta tempus eget, auctor.</p>

                    <h3>What is news?</h3>

                    <Carousel autoplay={About}>

                        <div>
                            <h3 style={contentStyle1}></h3>
                        </div>
                        <div>
                            <h3 style={contentStyle2}></h3>
                        </div>
                        <div>
                            <h3 style={contentStyle3}></h3>
                        </div>
                        <div>
                            <h3 style={contentStyle4}></h3>
                        </div>



                    </Carousel>
                    

                    <p>Nibh orci vivamus quisque nec, lorem justo, tempus pellentesque rutrum pharetra arcu — et sit vivamus curabitur nec lectus nam. Orci diam ornare justo diam orci urna: eu ornare arcu. Sem auctor, et justo et eu, ultricies donec enim, vulputate vitae, arcu vivamus mauris. Non maecenas, fusce massa sodales justo massa auctor nec in diam sem quisque, proin congue, nibh massa amet rutrum. Cursus metus nec ut nulla: eu amet eget risus porta auctor eros quisque lorem elementum enim sem ut arcu integer. Auctor leo massa nam porttitor nam morbi eros et, massa nibh amet cursus, fusce vivamus diam nibh sit porttitor integer.</p>



                    <p>Curabitur leo ornare donec mattis donec nec diam lorem vivamus mauris commodo lorem arcu congue sodales leo eu proin tempus urna quisque. Orci at rutrum in urna auctor risus integer curabitur amet — quisque: malesuada proin rutrum donec molestie ultricies rutrum. A vitae proin vitae magna porta pellentesque sapien sem eros odio, pellentesque, porttitor sagittis orci pellentesque integer bibendum eget commodo. Ligula pellentesque donec enim non vivamus nam amet eget morbi ligula morbi congue eu urna tempus quam nibh metus, odio pharetra. Amet proin sem donec pellentesque adipiscing, nibh duis, eget porttitor auctor sagittis metus in risus. Massa, congue sagittis morbi, magna sem pellentesque congue rutrum cursus. Ipsum nulla malesuada urna sagittis pharetra eu orci justo rutrum morbi: in sit arcu — eu sodales odio eu a sodales integer leo urna.</p>


                </div>
            </div>
        </div>

    )
}



export default About;