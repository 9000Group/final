import React from 'react';
import * as styles from './Contacts.module.css'

function Contacts() {

    return (
        <section className={styles.contacts}>
            <div className="container">
                <h2>Contacting News</h2>
                <div className={styles.wrapper}>
                    <form method="get" encType="multipart/form-data" className={styles.form}>

                        <fieldset>
                            <legend>General information</legend>
                            <div className="field">
                                Name<br />
                                <input required minLength="2" type="text" name="surtname" placeholder="Elon" />
                            </div>

                            <div className="field">
                                Surname<br />
                                <input required minLength="2" type="text" name="firstname" placeholder="Musk" />
                            </div>

                            <div className="field">
                                Date of Birth<br />
                                <input required type="date" />
                            </div>
                        </fieldset>

                        <fieldset>
                            <legend>Contact information</legend>
                            <div className="field">
                                Phone<br />
                                <input required type="tel" name="phone" placeholder="+......" />
                            </div>

                            <div className="field">
                                E-mail<br />
                                <input type="email" name="email" placeholder="@gmail.com" />
                            </div>

                            <div className="field">
                                City<br />
                                <input required minLength="2" type="text" name="city" placeholder="Toronto" />
                            </div>
                        </fieldset>

                        <fieldset className="height">
                            <legend>Message</legend>

                            <div className="field">
                                Your message<br />
                                <textarea name="message" placeholder="Enter text"></textarea>
                            </div>
                        </fieldset>

                        <button className={styles.butSend} type="submit">Send</button>
                        <button className={styles.butClear} type="reset">Clear</button>

                        <div className={styles.wrapper}>
                            <form method="get" encType="multipart/form-data" className={styles.form_map}>

                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2350.4617871706632!2d27.53804641603686!3d53. // eslint-disable-line90576954036114!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dbcff20d788617%3A0x42c199dab75e6b6b!2z0YPQuy4g0JrQsNC70YzQstCw0YDQuNC50YHQutCw0Y8sINCc0LjQvdGB0Lo!5e0!3m2!1sru!2sby!4v1634646715598!5m2!1sru!2sby" // eslint-disable-line
                                    width="100%" height="400" style={{ border: 0 }} allowfullscreen="" loading="lazy"></iframe>

                            </form>
                        </div>

                    </form>

                </div>

            </div>

        </section>


    )
}





export default Contacts;