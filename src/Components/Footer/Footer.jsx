import React from 'react';
import * as styles from './Footer.module.css'
import { NavLink } from 'react-router-dom';

function Footer() {

    return (
        <footer className={styles.footer}>
            <div className="footer_poster"> </div>
            <div className="footer__container">
                <div className="container">
                    <div className="footer__wrapper">

                        <div className={styles.footer__nav}>
                            <div className={styles.footer__nav__link}>
                                <NavLink to='/'>Top News </NavLink>
                                <NavLink to='/choosed-news'>Search News </NavLink>
                                <NavLink to='/about'>About Project</NavLink>
                                <NavLink to='contacts'>Contacts</NavLink>
                            </div>
                        </div>
                        <div className={styles.footer__contact}>

                            <ul>
                                <a href="Address: 75 Park place 8th floor New York United States">Address: 75 Park place 8th floor New York United States</a>
                                <p/>
                                <a href="mailto:E-mail: News@gmail.com"> E-mail: News@gmail.com </a>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>
        </footer>
    )
}

export default Footer