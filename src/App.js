import React, {useState, useEffect} from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom'
import Header from "./Components/Header/Header"
import Nav from "./Components/Nav/Nav"
import Search from "./Components/Sectiions/Search/Search"
import Slider from "./Components/Sectiions/Slider/Slider"
import TopNews from "./Components/Sectiions/TopNews/TopNews"
import ChoosedNews from "./Components/Sectiions/ChoosedNews/ChoosedNews"
import About from "./Components/Sectiions/About/About"
import Contacts from "./Components/Sectiions/Contacts/Contacts"
import Footer from "./Components/Footer/Footer"
import Store from './Context'
import './App.css';


function App() {
  const[topFetchLink, setTopFetchLink] = useState(localStorage.getItem('topFetchLink') || 'https://newsapi.org/v2/top-headlines?sources=cnn,bbc-news,associated-press,bloomberg,the-wall-street-journal&apiKey=c89f1df91cd34ce9807189edf86192b4')
  const[topDataStatus, setTopDataStatus] = useState(false)  
  const[topNews, setTopNews] = useState([])

  

  const[choosedFetchLink, setChoosedFetchLink] = useState('https://newsapi.org/v2/everything?q=covid&apiKey=c89f1df91cd34ce9807189edf86192b4')
  const[choosedDataStatus, setChoosedDataStatus] = useState(false)  
  const[choosedNews, setChoosedNews] = useState([])

  const defaultPublishers = 'Fox Business,TechCrunch,The Wall Street Journal,Yahoo Entertainment,CNBC'
  const[country, setCountry] = useState('no country')
  const[publishers, setPublishers] = useState(defaultPublishers)
  const[choosedPublishers, setChoosedPublishers] = useState('default publisers')

  const getData = (link, set) => {
    fetch(link)
    .then((res) => {
      return res.text()
    })
    .then((data) => {
      set(data)
    })    
  }


  const setTop = (data) => {
    setTopNews(JSON.parse(data).articles)
    setTopDataStatus(true)
  }
  
  const setChoosed = (data) => {
    setChoosedNews(JSON.parse(data).articles)
    setChoosedDataStatus(true)
  }

  useEffect(() => {
    if (topDataStatus === false) getData(topFetchLink, setTop)   
  }, [topDataStatus, topFetchLink])

  useEffect(() => {
    if (choosedDataStatus === false) getData(choosedFetchLink, setChoosed)   
  }, [choosedDataStatus, choosedFetchLink])  
  
  return (
    <BrowserRouter >
      <Store.Provider value={{topNews}}>
        <Header/>

        <Nav/>      

        <Switch>        
          <Route exact path='/'>
            <Slider topFetchLink={topFetchLink} topDataStatus={topDataStatus} setTopFetchLink={setTopFetchLink} setTopDataStatus={setTopDataStatus}/>
            <TopNews/>
          </Route>

          <Route path='/choosed-news'>
            <Search choosedFetchLink={choosedFetchLink} choosedDataStatus={choosedDataStatus} setChoosedFetchLink={setChoosedFetchLink} setChoosedDataStatus={setChoosedDataStatus}/>
            <ChoosedNews choosedNews={choosedNews}/>
          </Route>

          <Route path='/about'>
            <About/>
            
          </Route>

          <Route path='/contacts'>
          
            <Contacts/>
          
          </Route>

        </Switch>

        <Footer/>
      </Store.Provider>
    </BrowserRouter>
  );
}

export default App;


